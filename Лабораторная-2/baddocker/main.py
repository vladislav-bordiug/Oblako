from mpmath import fac, mpf, exp
import math
from scipy.special import erf
Phi = lambda x: erf(x / 2 ** 0.5) / 2
nn = [100,1000,10000]
pp = [0.001,0.01,0.1,0.25,0.5]
for n in nn:
    for p in pp:
        print('n =', n, 'p =', p)
        left = math.ceil(n / 2 - (n * p * (1 - p)) ** 0.5)
        right = math.floor(n / 2 + (n * p * (1 - p)) ** 0.5)
        p_1 = sum([fac(n)/(fac(k)*fac(n-k)) * mpf(p) ** k * mpf(1 - p) ** (n - k) for k in range(left, right + 1)])
        p_2 = sum([fac(n)/(fac(k)*fac(n-k)) * mpf(p) ** k * mpf(1 - p) ** (n - k) for k in range(6)])
        if (n + 1) * p % 1 == 0:
            p1 = fac(n) / (fac((n + 1) * p) * fac(n - (n + 1) * p)) * mpf(p) ** ((n + 1) * p) * mpf(1 - p) ** (n - (n + 1) * p)
            p2 = fac(n)/(fac((n + 1) * p - 1)*fac(n-(n + 1) * p - 1)) * mpf(p) ** ((n + 1) * p - 1) * mpf(1 - p) ** (n - (n + 1) * p - 1)
            if p1 > p2:
                kmax = (n + 1) * p
            else:
                kmax = (n + 1) * p - 1
        else:
            kmax = int((n + 1) * p)
        p_3 = fac(n)/(fac(kmax)*fac(n-kmax)) * mpf(p) ** kmax * mpf(1 - p) ** (n - kmax)
        print('P(Sn in [n/2-sqrt(npq),n/2+sqrt(npq)]) =', p_1, '- Бернулли')
        print('P(Sn<=5) =', p_2, '- Бернулли')
        print('max(P(Sn=k)) =', p_3, '- Бернулли')
        if n*p > 100:
            p1 = Phi((right - n * p) / (n * p * (1 - p)) ** 0.5) - Phi((left - n * p) / (n * p * (1 - p)) ** 0.5)
            p2 = Phi((5 - n * p) / (n * p * (1 - p)) ** 0.5) - Phi((0 - n * p) / (n * p * (1 - p)) ** 0.5)
            p3 = (math.e ** (-((kmax - n * p) / (n * p * (1 - p)) ** 0.5) ** 2 / 2)) / (n * p * (1 - p) * 2 * math.pi) ** 0.5
            print('P(Sn in [n/2-sqrt(npq),n/2+sqrt(npq)]) =', p1, '- Муавра-Лапласа;','Погрешность: ', abs(p1-p_1))
            print('P(Sn<=5) =', p2, '- Муавра-Лапласа;', 'Погрешность: ', abs(p2-p_2))
            print('max(P(Sn=k)) =', p3, '- Муавра-Лапласа;', 'Погрешность: ', abs(p3-p_3))
        elif n*p < 10:
            p1 = sum([exp(-n*p) * mpf(n*p)**k/fac(k) for k in range(left, right + 1)])
            p2 = sum([exp(-n*p) * mpf(n*p)**k/fac(k) for k in range(6)])
            p3 = exp(-n*p) * mpf(n*p)**kmax/fac(kmax)
            print('P(Sn in [n/2-sqrt(npq),n/2+sqrt(npq)]) =', p1, '- Пуассона;','Погрешность: ', abs(p1-p_1))
            print('P(Sn<=5) =', p2, '- Пуассона;', 'Погрешность: ', abs(p2-p_2))
            print('max(P(Sn=k)) =', p3, '- Пуассона;', 'Погрешность: ', abs(p3-p_3))
        else:
            p1 = Phi((right - n * p) / (n * p * (1 - p)) ** 0.5) - Phi((left - n * p) / (n * p * (1 - p)) ** 0.5)
            p2 = sum([exp(-n*p) * mpf(n*p)**k/fac(k) for k in range(left, right + 1)])
            p3 = Phi((5 - n * p) / (n * p * (1 - p)) ** 0.5) - Phi((0 - n * p) / (n * p * (1 - p)) ** 0.5)
            p4 = sum([exp(-n*p) * mpf(n*p)**k/fac(k) for k in range(6)])
            p5 = (math.e ** (-((kmax - n * p) / (n * p * (1 - p)) ** 0.5) ** 2 / 2)) / (n * p * (1 - p) * 2 * math.pi) ** 0.5
            p6 = exp(-n*p) * mpf(n*p)**kmax/fac(kmax)
            print('P(Sn in [n/2-sqrt(npq),n/2+sqrt(npq)]) =', p1, '- Муавра-Лапласа;','Погрешность: ', abs(p1-p_1))
            print('P(Sn in [n/2-sqrt(npq),n/2+sqrt(npq)]) =', p2, '- Пуассона;','Погрешность: ', abs(p2-p_1))
            print('P(Sn<=5) =', p3, '- Муавра-Лапласа;','Погрешность: ', abs(p3-p_2))
            print('P(Sn<=5) =', p4, '- Пуассона;','Погрешность: ', abs(p4-p_2))
            print('max(P(Sn=k)) =', p5, '- Муавра-Лапласа;','Погрешность: ', abs(p5-p_3))
            print('max(P(Sn=k)) =', p6, '- Пуассона;','Погрешность: ', abs(p6-p_3))
        print()