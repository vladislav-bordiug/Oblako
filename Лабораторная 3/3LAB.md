# Лабораторная работа 3

Сделать, чтобы после пуша в ваш репозиторий автоматически собирался докер образ и результат его сборки сохранялся куда-нибудь.

## Что мы сделали

Был сделан специальный [репозиторий](https://gitlab.com/vladislav-bordiug/oblakolab3) для этой лабы.
Мы сделали так, чтобы при пуше в этот репозиторий автоматически собирался Docker из Лабораторной 2 и пушился на dockerhub.

## Установка переменных (variables)

Для начала были созданы четыре переменные в настройках CI/CD:

```CI_REGISTRY``` - docker.io

```CI_REGISTRY_IMAGE``` - index.docker.io/username/image_name, в нашем случае username = vladislavbordiug, image_name = oblako

```CI_REGISTRY_PASSWORD``` - token с dockerhub

```CI_REGISTRY_USER``` - username c dockerhub = vladislavbordiug

## Написание yml файла

Была настроена CI/CD configuration с yml файлом со следующим содержанием:

```
docker-build-master:
  image: docker:latest
  stage: build
  services:
    - docker:dind
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - docker build --pull -t "$CI_REGISTRY_IMAGE" .
    - docker push "$CI_REGISTRY_IMAGE"
  only:
    - main
```

В данном случае при push в ветку main происходит login на dockerhub, сборка (build) образа Docker и его push на dockerhub.

## Результаты

Был сделан commit, изменяющий md файлик в репозитории:

![Alt text](image.png)

Как видно, pipeline успешно выполнился.

![Alt text](image-1.png)

![Alt text](image-2.png)

В итоге образ успешно запушился на dockerhub:

![Alt text](image-3.png)

![Alt text](image-4.png)

## Заключение

В результате проделанной работы было создан репозиторий, при пуше в который Docker образ из Лабораторной 2 собирается и пушится на dockerhub.
